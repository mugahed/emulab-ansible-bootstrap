#!/bin/sh

DIRNAME=`dirname $0`
REPO=/local/repository
OURDIR=/local/setup
SWAPPER=`geni-get user_urn | cut -f4 -d+`

#
# Our default configuration
#
DO_PACKAGE_INSTALL=1
DO_PACKAGE_UPGRADE=0
DO_SECURITY_UPGRADE=0
DO_DIST_UPGRADE=0
DO_PACKAGE_UPDATE=1

#
# Use sudo at times if we are not root.
#
if [ -z "$EUID" ]; then
    EUID=`id -u`
fi
SUDO=
if [ ! $EUID -eq 0 ] ; then
    SUDO=sudo
fi

[ ! -d $OURDIR ] \
    && ($SUDO mkdir -p $OURDIR && $SUDO chown $SWAPPER $OURDIR)

cd $OURDIR

[ -z "$PKGTYPE" ] \
    && grep -qi ID_LIKE=debian /etc/os-release \
    && PKGTYPE=deb
[ -z "$PKGTYPE" ] \
    && grep -qiE 'ID_LIKE=.*(rhel|redhat|fedora|centos)' /etc/os-release \
    && PKGTYPE=rpm

if [ -e $DIRNAME/lib/lib-${PKGTYPE}.sh ]; then
    . $DIRNAME/lib/lib-${PKGTYPE}.sh
else
    echo "ERROR: unrecognized OS type '$PKGTYPE'; aborting!"
    exit 1
fi

#
# Figure out the system python version.
#
python3 --version
if [ ! $? -eq 0 ]; then
    python --version
    if [ $? -eq 0 ]; then
	PYTHON=python
    else
	are_packages_installed python3
	success=`expr $? = 0`
	# Keep trying again with updated cache forever;
	# we must have python.
	while [ ! $success -eq 0 ]; do
	    make_package_cache
	    maybe_install_packages python3
	    success=$?
	done
	PYTHON=python3
    fi
else
    PYTHON=python3
fi
$PYTHON --version | grep -q "Python 3"
if [ $? -eq 0 ]; then
    PYVERS=3
    PIP=pip3
else
    PYVERS=2
    PIP=pip
fi
PYTHONBIN=`which $PYTHON`
