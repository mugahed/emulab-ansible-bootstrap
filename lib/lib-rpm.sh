#!/bin/sh

update_package_cache() {
    if [ ! -f $OURDIR/package-cache-updated -a "${DO_PACKAGE_UPDATE}" = "1" ]; then
	yum makecache
	touch $OURDIR/package-cache-updated
    fi
}

are_packages_installed() {
    retval=1
    while [ ! -z "$1" ] ; do
	rpm --quiet -q "$1"
	if [ ! $? -eq 0 ] ; then
	    retval=0
	fi
	shift
    done
    return $retval
}

maybe_install_packages() {
    update_package_cache
    if [ ! ${DO_PACKAGE_UPGRADE} -eq 0 ] ; then
        # Just do an install/upgrade to make sure the package(s) are installed
	# and upgraded; we want to try to upgrade the package.
	yum install -y $@
    else
	# Ok, check if the package is installed; if it is, don't install.
	# Otherwise, install (and maybe upgrade, due to dependency side effects).
	retval=0
	while [ ! -z "$1" ] ; do
	    are_packages_installed $1
	    if [ $? -eq 0 ]; then
		yum install -y $1
		retval=`expr $retval \| $?`
	    fi
	    shift
	done
	return $retval
    fi
}
