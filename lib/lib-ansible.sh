#!/bin/sh

#
# Our default configuration.
#
USE_SYSTEM_ANSIBLE=1
ANSIBLE_VERSION=
EMULAB_COLLECTION=git+https://gitlab.flux.utah.edu/emulab/ansible/emulab-common.git

[ ! -d $OURDIR/venv ] && mkdir -p $OURDIR/venv

install_system_ansible() {
    if [ $PKGTYPE = deb ]; then
	maybe_install_packages software-properties-common ${PYTHON}-pip
	$SUDO add-apt-repository --yes --update ppa:ansible/ansible
    fi
    if [ -n "$ANSIBLE_VERSION" ]; then
	maybe_install_packages ansible-${ANSIBLE_VERSION}
    else
	maybe_install_packages ansible
    fi
}

install_virtualenv() {
    if [ $PKGTYPE = deb ]; then
	maybe_install_packages virtualenv ${PYTHON}-pip \
	    || (echo "ERROR: failed to install virtualenv; aborting!" ; false ; return 1)
    else
	maybe_install_packages python3-virtualenv \
	    || maybe_install_packages python-virtualenv \
	    || maybe_install_packages python-virtualenv \
	    || (echo "ERROR: failed to install python\*-virtualenv; aborting!" ; false; return 1)
    fi
}

make_ansible_virtualenv() {
    name=$1
    vers=$2
    requirements=$3
    if [ -z "$name" ]; then
	name="default"
    fi
    if [ -z "$vers" ]; then
	vers=${ANSIBLE_VERSION}
    fi

    virtualenv -p $PYTHONBIN $OURDIR/venv/$name
    if [ ! $? -eq 0 ]; then
	echo "ERROR: make_ansible_virtualenv failed; aborting!"
	false
	return 1
    fi
    . $OURDIR/venv/$name/bin/activate
    pip install --upgrade pip
    if [ -n "$requirements" ]; then
	pip install -r $requirements
    fi
    #apath=`find $OURDIR/venv/$1 -name ansible-playbook`
    apath=`which ansible-playbook`
    if [ ! $? -eq 0 -o -z "$apath" ]; then
	if [ -n "$vers" ]; then
	    pip install ansible==$vers
	else
	    pip install ansible
	fi
    fi
}

activate_ansible_virtualenv() {
    name=$1
    if [ -z "$name" ]; then
	name="default"
    fi
    . $OURDIR/venv/$name/bin/activate
}

deactivate_ansible_virtualenv() {
    deactivate
}

get_ansible() {
    name=$1
    if [ -z "$name" ]; then
	name="default"
    fi
    if [ -n "$USE_SYSTEM_ANSIBLE" -a $USE_SYSTEM_ANSIBLE -eq 1 ]; then
	which ansible-playbook
    else
	activate_ansible_virtualenv $name
	#find $OURDIR/venv/$name -name ansible-playbook
	which ansible-playbook
    fi
}

get_ansible_galaxy() {
    name=$1
    if [ -z "$name" ]; then
	name="default"
    fi
    if [ -n "$USE_SYSTEM_ANSIBLE" -a $USE_SYSTEM_ANSIBLE -eq 1 ]; then
	which ansible-galaxy
    else
	activate_ansible_virtualenv $name
	#find $OURDIR/venv/$name -name ansible-galaxy
	which ansible-galaxy
    fi
}

put_ansible() {
    name=$1
    if [ -z "$name" ]; then
	name="default"
    fi
    if [ -z "$USE_SYSTEM_ANSIBLE" -o $USE_SYSTEM_ANSIBLE -eq 0 ]; then
	deactivate_ansible_virtualenv $name
    fi
}
